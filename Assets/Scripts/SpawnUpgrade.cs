﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TurretUpgradeList
{
	public GameObject[] TurretUpgrade;
	public string Name;
	public int Price;	
}

public class SpawnUpgrade : MonoBehaviour
{
	public List <TurretUpgradeList> UpgradeTurrets = new List<TurretUpgradeList>();
	private int newGold;
	private Vector3 up;
	private GoldCounter g;
	private GameObject player;
	private bool canDestroy = false;
	
	void Start()
	{
		up = new Vector3 (this.transform.position.x, 5.0f, this.transform.position.z);
		player = GameObject.FindGameObjectWithTag("Player");
		g = player.GetComponent<GoldCounter>();     
	}
	
	public void SpawnTower()
	{
        if (g.GoldCount() > -UpgradeTurrets[0].Price)
        {
            newGold = g.GoldCount() - UpgradeTurrets[0].Price;
            player.BroadcastMessage("SetGold", newGold);
            Instantiate(UpgradeTurrets[0].TurretUpgrade[0], up, transform.rotation);
        }		
	}
}
