﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConditionChecker : MonoBehaviour 
{
    private GameObject[] enemies;
    private GameObject[] bossEnemies;
    private EnemySpawner wave;

	void Start () 
	{
        wave = GetComponent<EnemySpawner>();
	}
	
	void Update () 
	{
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        bossEnemies = GameObject.FindGameObjectsWithTag("BossEnemy");
     
            if (enemies.Length == 0 && bossEnemies.Length == 0 && wave.lastWave == true)
            {
                BroadcastMessage("OpenWindow");
            }
    }

    void OpenWindow()
    {
        Application.LoadLevel(3);
    }
}
