﻿using UnityEngine;
using System.Collections;

public class SlowEffect : MonoBehaviour 
{
	public float FarArea;
	public float Damage;
	private Transform areaDamage;
	private NavMeshAgent newSpeed;
	private float timer;

void Awake()
	{
		areaDamage = transform;
	}

	void OnTriggerEnter()
	{
		Collider[] colls = Physics.OverlapSphere(areaDamage.position, FarArea);
		foreach (Collider col in colls)
		{
			if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "BossEnemy")
			{
				newSpeed = col.gameObject.GetComponent<NavMeshAgent>();
				newSpeed.speed = 2;
				col.BroadcastMessage("DecreaseHealth", Damage);
	            Destroy(this.gameObject, 2f);
				timer += Time.deltaTime;
	        }
    	}
    }
}
