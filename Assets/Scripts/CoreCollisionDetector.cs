﻿using UnityEngine;
using System.Collections;

public class CoreCollisionDetector : MonoBehaviour
{
    private GameObject UI;

    void Start()
    {
        UI = GameObject.FindGameObjectWithTag("MainCamera");
    }
	
	void OnTriggerEnter(Collider f)
	{
		if (f.gameObject.tag == "Enemy" || f.gameObject.tag == "BossEnemy")
		{
			Destroy(f.gameObject);
			UI.BroadcastMessage("Decrease", 20f);       
		}
	}
}
