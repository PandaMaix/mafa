﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealthBarDecrease : MonoBehaviour
{
	private float cH, mH;
	private GameObject[] enemies;
    private GameObject[] bossEnemies;
    private List<GameObject> allEnemies = new List<GameObject>();
	Health hs;
	private bool isBurning;
	private float timeElapsed;
	
	void Start ()
	{
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		foreach (GameObject enemy in enemies)
		{
			hs = enemy.GetComponent<Health>();
            cH = hs.CurHealth;
            mH = hs.MaxHealth;
            cH = mH;	
		}

        bossEnemies = GameObject.FindGameObjectsWithTag("BossEnemy");
        foreach (GameObject boss in bossEnemies)
        {
            hs = boss.GetComponent<Health>();
            cH = hs.CurHealth;
            mH = hs.MaxHealth;
            cH = mH;
        }

        allEnemies.AddRange(enemies);
        allEnemies.AddRange(bossEnemies);
	}
	
	void Update()
	{
		if (isBurning)
		{
			timeElapsed += Time.deltaTime;
		}
		
		if (timeElapsed > 3)
		{
			timeElapsed = 0;
		}
	}
	
	public void DecreaseHealth(float damage)
	{
        if (cH > 0)
        {
            cH -= damage;
            float calcHealth = cH / mH;
            hs.BroadcastMessage("UpdateCurHealth", calcHealth);
            BroadcastMessage("UpdateBar", calcHealth);
        }		
	}
	
	public void FireDamage(float damage)
	{
		isBurning = true;
		cH -= damage;
		float calcHealth = cH / mH;
        hs.BroadcastMessage("UpdateCurHealth", calcHealth);
		BroadcastMessage("UpdateBar", calcHealth);
	}
	
	public void UpdateBar(float myHealth)
	{
		hs.CurHealth = myHealth;
		transform.localScale = new Vector3(myHealth, transform.localScale.y, transform.localScale.z);
	}
}
