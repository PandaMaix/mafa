﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GodPowers : MonoBehaviour
{
    private GoldCounter g;
    private GameObject player;
    private int newGold;
    private GameObject[] allEnemies;
    private bool canUse = false;
    private bool isUsed = false;
    private float regenTimer = 0;
    private float regenAmt;
    public Button button;

	void Start ()
    {
        g = gameObject.GetComponent<GoldCounter>();
	}

	void Update ()
    {
        if (g.GoldCount() >= 2000)
            canUse = true;
        else
            canUse = false;

        if (isUsed)
        {
            regenTimer += Time.deltaTime;
        }

        if (regenTimer >= regenAmt)
        {
            isUsed = false;
            button.interactable = true;
        }
	}

    public void ApocalypsePower()
    {
        regenAmt = 10.0f;
        if (canUse)
        {            
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach(GameObject enemy in allEnemies)
            {
                Destroy (enemy);
            }
            newGold = g.GoldCount() - 2000;
            g.SendMessage("SetGold", newGold);
            button.interactable = false;
            isUsed = true;
        }        
    }
}
