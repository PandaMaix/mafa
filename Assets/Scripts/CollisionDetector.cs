﻿using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour
{
    public Transform target;

	void Update ()
    {
        if (target == null)
        {
            BroadcastMessage("StandBy");
        }
	}
    
   void OnTriggerStay(Collider o)
    {
        if (o.gameObject.tag == "Enemy" || o.gameObject.tag == "BossEnemy")
        {
            target = o.transform;
            this.transform.LookAt(o.transform.position);
            BroadcastMessage("Attack", 1f);                  
            
        }
    }
    
	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.transform == target)
		{
			if (target != null)
				target = null;
			BroadcastMessage("StandBy");
		}
	}
}
