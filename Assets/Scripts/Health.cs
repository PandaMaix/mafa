﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
public GameObject CoreObject;
public float CurHealth = 0f;
public float MaxHealth = 100f;

	void Start ()
    {
        CurHealth = MaxHealth;
        CoreObject = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update ()
    {	
		if (CurHealth <= 0 || CurHealth == 0)
		{
			CoreObject.BroadcastMessage("AddGold", 10);
			Destroy(this.gameObject);
        }
    }

    void UpdateCurHealth(float current)
    {
        CurHealth = current;
    }
}
