﻿using UnityEngine;
using System.Collections;

public class DOT : MonoBehaviour
{
    public float FarArea;
    public float Damage;
    private Transform areaDamage;
    public GameObject explosion;
    Renderer r;

    void Awake()
    {
        areaDamage = transform;
        r = GetComponent<Renderer>();
    }

    void OnTriggerEnter()
    {
        Collider[] colls = Physics.OverlapSphere(areaDamage.position, FarArea);
        foreach (Collider col in colls)
        {
            if (col.gameObject.tag == "Enemy")
            {                
                col.BroadcastMessage("FireDamage", Damage);
                r.enabled = false;
                Instantiate(explosion, this.transform.position, this.transform.rotation);
                Destroy(this.gameObject, 3f);				
            }
        }
    }
}
