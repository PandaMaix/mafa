using UnityEngine;
using System.Collections;

public class TileMouseOver : MonoBehaviour 
{
	private Color highlightColor;
	private Color normalColor;
	private bool tileSelected = false;
	private TowerShop ts;
		
	void Start () 
	{
		normalColor = GetComponent<Renderer>().material.color;
		highlightColor = Color.cyan;
		ts = gameObject.GetComponent<TowerShop>();
	}
	
	void Update () 
	{
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition);
		RaycastHit hitInfo;
		
		if (Input.GetMouseButtonDown(0))
		{
			if (GetComponent<Collider>().Raycast(ray, out hitInfo, Mathf.Infinity) && ts.HasTower == false)
			{
					BroadcastMessage("SelectTower");
					GetComponent<Renderer>().material.color = highlightColor;
			}			
    		else
    		{
                GetComponent<Renderer>().material.color = normalColor;
    		}
		}
	}
}
