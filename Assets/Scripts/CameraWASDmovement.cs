﻿using UnityEngine;
using System.Collections;

public class CameraWASDmovement : MonoBehaviour
{
	void Update ()
    {
        float xAxisValue = Input.GetAxis("Horizontal");
        float zAxisValue = Input.GetAxis("Vertical");
        if (Camera.current != null)
        {
            Camera.current.transform.Translate(new Vector3(xAxisValue, zAxisValue, zAxisValue));
        }
    }
}
