﻿using UnityEngine;
using System.Collections;

public class EndScreen : MonoBehaviour 
{
    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        Application.LoadLevel(1);
    }
}
