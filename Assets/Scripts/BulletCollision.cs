﻿using UnityEngine;
using System.Collections;

public class BulletCollision : MonoBehaviour
{

   void OnTriggerEnter(Collider h)
    {
        if (h.gameObject.tag == "Bullet")
        {
            BroadcastMessage("DecreaseHealth", 5.0f);
        }

    }
}
