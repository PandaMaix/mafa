﻿using UnityEngine;
using System.Collections;

public class CoreHealthBarDecrease : MonoBehaviour 
{
	private float cH, mH;
	private GameObject core;
	Health hs;
	
	void Start ()
	{
		core = GameObject.Find("Castle");
		hs = core.GetComponent<Health>();
		cH = hs.CurHealth;
		mH = hs.MaxHealth;
		cH = mH;
	}
	
    public void Decrease(float damage)
	{
		cH -= damage;
		float calcHealth = cH / mH;
		BroadcastMessage("UpdateCoreHealthBar", calcHealth);
	}
	
	void UpdateCoreHealthBar (float myHealth)
	{
		hs.CurHealth = myHealth;
		transform.localScale = new Vector3(myHealth, transform.localScale.y, transform.localScale.z);
	}

    void Update()
    {
        if (cH <= 0)
        {
            Application.LoadLevel(2);
        }
    }

}
