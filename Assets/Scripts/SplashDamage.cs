﻿using UnityEngine;
using System.Collections;

public class SplashDamage : MonoBehaviour
{
    public float NearArea;
    public float MediumArea;
    public float FarArea;

    public float NearDamage;
    public float MediumDamage;
    public float FarDamage;

    private Transform areaDamage;

    void Awake()
    {
        areaDamage = transform;
    }

    void OnTriggerEnter()
    {
       Collider[] colls = Physics.OverlapSphere(areaDamage.position, FarArea);
        foreach (Collider col in colls)
        {
           
                if (col.gameObject.tag=="Enemy")
                {
                    float distance = Vector3.Distance(col.transform.position, areaDamage.position);
                    
                    if (distance <= NearArea)
                    {
                   		col.BroadcastMessage("DecreaseHealth", NearDamage);
                    }
                    
                 	else if (distance <= MediumArea)
                    {
                        col.BroadcastMessage("DecreaseHealth", MediumDamage);
                    }
					else
					{
						col.BroadcastMessage("DecreaseHealth", FarDamage);
					}
				Destroy(this.gameObject, 2f);
                }
        }
    }
} //code modified from http://answers.unity3d.com/questions/178902/area-effect-damage.html courtesy of overlord