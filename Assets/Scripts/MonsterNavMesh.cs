﻿using UnityEngine;
using System.Collections;

public class MonsterNavMesh : MonoBehaviour
{
    public Transform target;
	NavMeshAgent agent;

	void Update ()
	{
        agent = GetComponent<NavMeshAgent>();   
        agent.SetDestination (target.position);
	}
}
