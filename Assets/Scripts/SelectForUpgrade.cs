﻿using UnityEngine;
using System.Collections;

public class SelectForUpgrade : MonoBehaviour
{
	private Color highlightColor;
	private Color normalColor;
    private GameObject towerbase;
	private TowerShop ts;

    public Canvas UpgradeCanvas;

    void Start () 
	{
		normalColor = GetComponent<Renderer>().material.color;
		highlightColor = Color.red;
        towerbase = GameObject.FindGameObjectWithTag("Base");
        ts = this.gameObject.GetComponent<TowerShop>();

        UpgradeCanvas.enabled = false;
    }
	
	void Update () 
	{
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition);
		RaycastHit hitInfo;
		
		if (Input.GetMouseButtonDown(0))
		{
			if (GetComponent<Collider>().Raycast(ray, out hitInfo, Mathf.Infinity))
			{
				GetComponent<Renderer>().material.color = highlightColor;
                OpenUpgradesGUI(transform.parent.gameObject);				
			}			
			else
			{
				GetComponent<Renderer>().material.color = normalColor;
			}
		}
	}

    public void OpenUpgradesGUI(GameObject toDelete)
    {
        Destroy(toDelete, 2f);
        UpgradeCanvas.enabled = true;
    }
}
