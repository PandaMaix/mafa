﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TurretList
{
	public GameObject[] Turret;
	public string Name;
	public int Price;	
}

public class SpawnTurret : MonoBehaviour 
{
	public List <TurretList> Turrets = new List<TurretList>();
	private int newGold;
	private TowerShop ts;
	private Vector3 up;
	private GoldCounter g;
    private GameObject player;
	
	void Start()
	{
		ts = gameObject.GetComponent<TowerShop>();
		up = new Vector3 (this.transform.position.x, 5.0f, this.transform.position.z);
        player = GameObject.FindGameObjectWithTag("Player");
        g = player.GetComponent<GoldCounter>();     
	}
	
	public void SpawnArrowTower()
	{
        if (g.GoldCount() >= Turrets[0].Price)
        {
            newGold = g.GoldCount() - Turrets[0].Price;
            player.BroadcastMessage("SetGold", newGold);
            Instantiate(Turrets[0].Turret[0], up, transform.rotation);
            ts.HasTower = true;
            ts.DeselectTower();
        }	
	}
	
	public void SpawnFireTower()
	{
        if (g.GoldCount() >= Turrets[1].Price)
        {
            newGold = g.GoldCount() - Turrets[1].Price;
            player.BroadcastMessage("SetGold", newGold);
            Instantiate(Turrets[1].Turret[0], up, transform.rotation);
            ts.HasTower = true;
            ts.DeselectTower();
        }
	}
	
	public void SpawnIceTower()
	{
        if (g.GoldCount() >= Turrets[2].Price)
        {
            newGold = g.GoldCount() - Turrets[2].Price;
            player.BroadcastMessage("SetGold", newGold);
            Instantiate(Turrets[2].Turret[0], up, transform.rotation);
            ts.HasTower = true;
            ts.DeselectTower();
        }
	}
	
	public void SpawnCannonTower()
	{
        if (g.GoldCount() >= Turrets[3].Price)
        {
            newGold = g.GoldCount() - Turrets[3].Price;
            player.BroadcastMessage("SetGold", newGold);
            Instantiate(Turrets[3].Turret[0], up, transform.rotation);
            ts.HasTower = true;
            ts.DeselectTower();
        }
	}
}
