﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoldCounter : MonoBehaviour
{
	public int CollectedGold = 70;
	public Text GoldDisplay;

	void Start()
	{
		ShowGold();
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Alpha0))
		{
			AddGold(1000);
		}
	}
	
	public void AddGold(int count)
	{	
		CollectedGold += count;
		ShowGold();
	}	
	
	public int GoldCount()
	{
		return CollectedGold;
	}
	
	public void SetGold(int nGold)
	{
		CollectedGold = nGold;
		ShowGold();
	}
	
	public void ShowGold()
	{
		GoldDisplay.text = CollectedGold.ToString();
	}
}
