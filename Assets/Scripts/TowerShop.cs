﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowerShop : MonoBehaviour 
{
	public bool HasTower = false;
    public Canvas ShopCanvas;
   
	void Start ()
	{
        ShopCanvas.enabled = false;
	}
    
	void SelectTower()
	{
		OpenShopGUI();
	}
	
	public void DeselectTower()
	{
        ShopCanvas.enabled = false;
    }
    
    void OpenShopGUI ()
	{
        ShopCanvas.enabled = true;
	}
}
