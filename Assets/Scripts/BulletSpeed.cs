﻿using UnityEngine;
using System.Collections;

public class BulletSpeed : MonoBehaviour
{
	void Update ()
    {
        transform.Translate(Vector3.forward * 40f * Time.deltaTime);
        Destroy(this.gameObject, 2f);
    }
}
