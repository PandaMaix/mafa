﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuScript : MonoBehaviour 
{
	public Canvas ExitMenu;
	public Canvas InstructMenu;
    public Canvas InstructMenu2;
	public Button PlayButton;
	public Button ExitButton;
	public Button InstructButton;
    public Button Next;

	void Start () {
		ExitMenu = ExitMenu.GetComponent<Canvas> ();
		InstructMenu = InstructMenu.GetComponent<Canvas> ();
        InstructMenu2 = InstructMenu2.GetComponent<Canvas>();
		PlayButton = PlayButton.GetComponent<Button> ();
		ExitButton = ExitButton.GetComponent<Button> ();
		InstructButton = InstructButton.GetComponent<Button> ();
       Next = Next.GetComponent<Button>();
		ExitMenu.enabled = false;
		InstructMenu.enabled = false;
        InstructMenu2.enabled = false;
	}
	public void ExitPress()
    {
		ExitMenu.enabled = true;
		InstructMenu.enabled = false;
        InstructMenu2.enabled = false;
		PlayButton.enabled = false; 
		ExitButton.enabled = false;
		InstructButton.enabled = false;
	}

    public void NextPress()
    {
        ExitMenu.enabled =false;
        InstructMenu.enabled = false;
        InstructMenu2.enabled = true;
        PlayButton.enabled = false;
        ExitButton.enabled = true;
        Next.enabled = false;
        InstructButton.enabled = true;
    }

	public void InstructPress()
    {
		ExitMenu.enabled = false;
		InstructMenu.enabled = true;
        InstructMenu2.enabled = false;
		PlayButton.enabled = false; 
		ExitButton.enabled = false;
		InstructButton.enabled = true;
        Next.enabled = true;
	}

	public void NoPress()
    {
		ExitMenu.enabled = false;
		InstructMenu.enabled = false;
        InstructMenu2.enabled = false;
		PlayButton.enabled = true; 
		ExitButton.enabled = true;
		InstructButton.enabled = true;
	}

	public void StartGame()
    {
		Application.LoadLevel (1);
	}

	public void ExitGame()
    {
		Application.Quit();
	}
}