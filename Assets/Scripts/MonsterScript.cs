﻿using UnityEngine;
using System.Collections;

public class MonsterScript : MonoBehaviour 
{
	public Transform[] 	TWaypoints;
	private int curIdx = 0;

	void Start () 
    {
		this.transform.position =  new Vector3(TWaypoints[curIdx].position.x, TWaypoints[curIdx].position.y, TWaypoints[curIdx].position.z);
	}
	
    void Update () 
    {

		Vector3 eTargetPos =  new Vector3(TWaypoints[curIdx].position.x, TWaypoints[curIdx].position.y, TWaypoints[curIdx].position.z);
		
		if(Vector3.Distance(this.transform.position, eTargetPos) <= 0.1f)
		{
			if(TWaypoints.Length-1 > curIdx)
				curIdx++;
			else
				curIdx = 0;
		}
		this.transform.position = Vector3.MoveTowards( this.transform.position, eTargetPos , 10f * Time.deltaTime);
	}
}
