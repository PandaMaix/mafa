﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class MonsterSpawnInfo
{
	public GameObject Enemy;
	public int EnemyCount;
}

[System.Serializable]
public class Wave
{
    public GameObject[] E;
	public int WaveCount;
}

public class EnemySpawner : MonoBehaviour
{
	public List<Wave> Waves = new List<Wave>();  
	private int index = 0;
	public float SpawnWait;
	public float StartWait;
    public bool lastWave = false;
    [SerializeField]
    private GameObject Go;

	void Start ()
	{
		StartCoroutine (SpawnWaves ());
	}
		
	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds(StartWait);
		while(true)
		{
			for (int i = index; i < Waves.Count; i++)
			{
				Instantiate(Waves[Random.Range(0,2)].E[Random.Range(0,1)],this.transform.position, Quaternion.identity);
				yield return new WaitForSeconds (SpawnWait);	
			}
			Waves[index].WaveCount +=1;
			
			if (Waves[index].WaveCount % 5 == 0)
			{	
				Instantiate(Go,this.transform.position, Quaternion.identity);
            }
            yield return new WaitForSeconds(2f);
		}		
	}

    void Update()
    {
        if (Waves[index].WaveCount == 15)
        {
            lastWave = true;
            StopAllCoroutines();
        }
    }
}
