﻿using UnityEngine;
using System.Collections;

public class FireShot : MonoBehaviour
{
    public GameObject Bullet;
    public GameObject BarrelPos;
    public float FireRate = 0.5f;
    private Vector3 Pos;
    private bool isAttacking;
    private float timeElapsed;
    
    void Start()
    {
        Pos = BarrelPos.GetComponent<Transform>().position;
    }

    
    void Update()
    {
        if (isAttacking)
        {
            timeElapsed += Time.deltaTime;
        }

        if (timeElapsed > FireRate)
        {
            Instantiate(Bullet, Pos, transform.rotation);
            timeElapsed = 0;
        }
    }

    void Attack()
    {
        isAttacking = true;
    }

    void StandBy()
    {
        isAttacking = false;
    }
}

